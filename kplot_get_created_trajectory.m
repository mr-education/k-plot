function [k, gradobj] = kplot_get_created_trajectory(idTraj, pathTrajData);
% Loads previously created trajectory from file and returns it alongside
% gradient object
%
% [k, gradobj] = kplot_get_created_trajectory(idTraj, pathTrajData);
%
% IN
%
% OUT
%
% EXAMPLE
%   kplot_get_created_trajectory
%
%   See also
%
% Lars Kasper (c) 2020-03-12 

switch idTraj
    case {2,9}
        fileGradient = 'gradientsFull.txt';
    otherwise
        fileGradient = 'gradients.txt';
end
fileGradient = fullfile(pathTrajData, num2str(idTraj), fileGradient);

gs = GradientSystem();
gs.MAX_GRADIENT = 0.031;
gradobj = GradientWriter(gs,GradientWriterInput());
gradobj.loadFromTxt(fileGradient);

k = squeeze(gradobj.GetK());
