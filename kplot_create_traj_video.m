%function output = kplot_create_traj_video(input)
% creates video for traversing k-space
%
%   output = kplot_create_traj_video(input)
%
% IN
%
% OUT
%
% EXAMPLE
%   kplot_create_traj_video
%
%   See also
%
% Lars Kasper (c) 2020-02-13

%% load trajectory
isVerbose = true; % if true, also plot a figure with the trajectory itself
doCreateVideo = true;

pathProject = 'C:\Users\kasperla\polybox\Private\Toronto\Research\2020-02-13-Talk-Spirals-Labmeeting\k-plot';
pathTrajData = fullfile(pathProject, 'data/kplot');
idTraj = 2; % 1,3,4,5 = spiral, 2 = epi

[k, gradobj] = kplot_get_created_trajectory(idTraj, pathTrajData);

% video options
zoomTime_ms = [0 60];
nCropSamplesEnd = 100;
nFrames = 500;%1000%%100;


%% some more diagnostic plots
if isVerbose
    figure('Name', 'k over time');
    gradobj.plot('k');
    figure('Name', 'k parametric');
    gradobj.plot('kParametric');
end

%% prepare for video
zoomK = [min(k(:,1)), max(k(:,1)), min(k(:,2)), max(k(:,2))];

%% Create Video

if doCreateVideo
    
nSamples = size(k,1) - nCropSamplesEnd;

iEndSamples = linspace(1,nSamples, nFrames);

stringTitle = 'Video of Trajectory Evolution';

v = VideoWriter(sprintf('traj%d.avi', idTraj));
open(v);

gs = gradobj.gs;

fh = [];
hs = [];
for iFrame = 1:nFrames
    [fh, hs] = plot_traj_grad_slew(k(1:iEndSamples(iFrame),:), ...
        'dt', gs.GRADIENT_DWELL,...
        'gmax', gs.MAX_GRADIENT, ...
        'positionLegend', 'NorthEast', ...
        'zoomTime', zoomTime_ms/1000, ...
        'zoomK', zoomK, ...
        'fh', fh, 'hs', hs);
    drawnow;
    frame = getframe(gcf);
    writeVideo(v,frame);
end

close(v);

end