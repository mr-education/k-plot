% GradientFileDescriptor
% Describes file format for gradient text files
% described is the content of row iRow as x(iRow)

% Row Index     Name            Description
% 1             idxVersion      text file format version (typically #4)
% 2             nSamplesTotal   Total number of gradient Samples (nSamplesPerInterleaf x nInterleaves x nDims)
% 3             dtGradient      Gradient Dwell time (seconds)
% 4             nSamplesPerInterleaf 
%                               Number of gradient samples per interleaf
% 5             nInterleaves    Number of Interleaves
% 6             nDims           Number of gradient dimensions (2 = x,y, 3 = x,y,z)
% 7             TE              Time to k-space center (seconds), typically echo time
% 8             tAcq            Duration per acquisition window (ADC) (seconds)
% 9             nSamplesAcq     Number of samples per acquisition window
% 10            nAcq            Number of acquisitions within interleaf (e.g., echo images,
%                               split EPI lines w/o gradient ramps)
% 11            TRacq           spacing between multiple acquisition blocks (within one interleaf)         
% 12            delayAcq        Time from the onset of the gradient shape to the onset of acquisition (StartAcq). 
%                               Negative values mean the acq starts before the gradient.
% 13            tEchoShift      Time (seconds) that acqusition is shifted interleaf-to-interleaf. 
%                               Only relevant for multi-shot EPI to avoid sharp T2*-related k-space intensity jumps.
% 14-16         FOV             field of view (x,y,z) (meter)
% 17-19         dr = [dx,dy,dz] resolution (x,y,z) (meter) 
% 20            amplitudeGradient gradient normalization factor (in T/m)
%                               to be multiplied with normalized samples to
%                               retrieve actual gradient amplitude of
%                               sample
% 21            isBinary        boolean; if true, actual gradient samples
%                               will be in a different binary file, not this text file
% 22-end        normSamplesGradient 
%                               normalized gradient samples (values between -1 and +1), 
%                               samples are ordered following these
%                               dimensions:
%                               samplesPerInterleaf, idxInterleaf, idxGradientDimension, i.e.
%                               orderedSamplesGradient = ...
%                                   reshape(normGradSamples, nSamplesPerInterleaf, nInterleaves, nDims);
%
%                                     Gx_sample01_interleaf0l
%                                     ...
%                                     Gx_sampleNs_interleaf0l
%                                     ...
%                                     Gx_sample01_interleafNi
%                                     ...
%                                     Gx_sampleNs_interleafNi
%                                     Gy_sample01_interleaf01
%                                     ...
%                                     Gy_sampleNs_interleafNi
%                                     Gz_sample01_interleaf01
%                                     ...
%                                     Gz_sampleNs_interleafNi
%         
        
                               
