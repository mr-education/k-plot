% Script spidi_plot_nominal_trajectories
% Plots nominal trajectories created by gradient creation
%
%  spidi_plot_nominal_trajectories
%
%
%   See also
 
% Author:   Lars Kasper
% Created:  2020-11-14
% Copyright (C) 2020 BRAIN-TO Lab, Techna Institute, UHN Toronto, Canada
%
% Please see LICENSE file for how to use items in this repository.
 
 
 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Load trajectory from gradients*.txt file and plot
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

idTraj = 513;
pathTrajData = 'C:\Users\kasperla\Documents\Code\BRAIN-To\k-plot\data\SPIDI_0001';
[k, gradobj] = kplot_get_created_trajectory(idTraj, pathTrajData);
% take only 1st interleaf
if ndims(k) > 2
    k = k(:,1,:);
end
[fh,hs, data] = plot_traj_grad_slew(k, 'dt', 10e-6, 'gmax', 40e-3, 'smax', 170);

%% Compute maximum spectral bandwidth that object spans while max gradients
% are active (i.e., sum of abs of all gradients

FOV = max(gradobj.gwi.fov);
gamma1H_kHz = gradobj.gs.GAMMA_1H/1e3;
maxSumAbsGradient = max(sum(abs(g),2));
maxObjectBandwidth_kHz = maxSumAbsGradient*FOV*gamma1H_kHz;
maxDwellTime_mus = 1e3/maxObjectBandwidth_kHz;

fprintf(['\n\nThe maximum object bandwidth during trajectory %d is: %3.0f kHz\n' ...
    '\tnecessitating an ADC dwell time of less than     : %2.1f mus'], ...
    idTraj, maxObjectBandwidth_kHz, maxDwellTime_mus);