% Plot previously created trajectories nicely
%
% IN
%
% OUT
%
% EXAMPLE
%   kplot_plot_created_trajectories
%
%   See also
%
% Lars Kasper (c) 2020-03-12
pathProject = 'C:\Users\kasperla\Documents\Code\BRAIN-To\k-plot';
pathTrajData = fullfile(pathProject, 'data/kplot');

doCreateInterleafAnimation = false;

nCropSamplesEnd = 65;


fh = [];
for idTraj = 20%3:5
    [k, gradobj] = kplot_get_created_trajectory(idTraj, pathTrajData);
    isSingleInterleaf = ndims(k) == 2;
    
    if doCreateInterleafAnimation
        v = VideoWriter(sprintf('traj%d_interleaves.avi', idTraj));
        v.FrameRate = 2;
        open(v);
    end
    
    if isSingleInterleaf
        k((end-nCropSamplesEnd+1):end,:) = [];
    else
        k((end-nCropSamplesEnd+1):end,:,:) = [];
    end
    %% plot with GradientWriter methods
    figure('Name', 'k over time');
    gradobj.plot('k');
    figure('Name', 'k parametric');
    gradobj.plot('kParametric');
    
    %% Plot with Lars' old GradientData (incl. spectrum) method
    plot_gradient_spectrum(gradobj);    
    
    %% nicer plot with colors for each interleaf
    stringTitle = sprintf('Traj %d, k parametric, interleaves', idTraj);
    fh(end+1) = figure('Name', stringTitle);
    
    % swap dims for single interleaf traj
    if ismatrix(k)
        k = permute(k, [1 3 2]);
    end
    
    
    for il = 1:size(k,2)
        plot(k(:,il,1),k(:,il,2),'.')
        hold all;
        if il==1 % create axes
            xlabel('k_x (rad/m)');
            ylabel('k_y (rad/m)');
            axis square;
        end
        if doCreateInterleafAnimation
            drawnow;
            frame = getframe(gcf);
            writeVideo(v,frame);
        end
    end
    
    if doCreateInterleafAnimation
        close(v);
    end
    %title(stringTitle);
    
end
%save_plots_abstract(fh, pathTrajData);
