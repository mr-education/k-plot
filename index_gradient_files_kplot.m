% id, FOVm, FOVp, FOVs, dxm, dxp, dxs, Rp, Rs, nIl, maxG, maxSR, maxTacq, in/out
% OR (# added echoes), type, scheme3D, nPlanesPerShot
% InOut/OutIn-Trajektorien
% For 2D trajectories, set FOVs and dxs to zero.
% scheme3D: 'etagere', 'stack' (of spirals),'yarnball','shells','cones',
% 'blippedCones','gaSpiral' (leave empty or 'stack' for 2D traj)


%% Demo for Talk 1mm spiral and
% comparative EPI with same k-space are (sqrt(4/pi) factor in resolution)
1, 0.22, 0.22, 0, 1e-3, 1e-3, 0, 4, 1, 1, 31e-3, 200, 0, out, minTime, stack, 1
2, 0.22, 0.22, 0, 1.128e-3, 1.128e-3, 0, 4, 1, 1, 31e-3, 200, 0, 0, EPI, stack, 1

%% 3T sequences to test, readouts about 30 ms
3, 0.22, 0.22, 0, 1.5e-3, 1.5e-3, 0, 1, 1, 2, 31e-3, 160, 0, out, minTime, stack, 1
4, 0.22, 0.22, 0, 1.2e-3, 1.2e-3, 0, 1, 1, 3, 31e-3, 160, 0, out, minTime, stack, 1
5, 0.22, 0.22, 0, 2.2e-3, 2.2e-3, 0, 1, 1, 1, 31e-3, 160, 0, out, minTime, stack, 1

%% 3T sequences to test, more daring versions (longer readouts)
6, 0.22, 0.22, 0, 1e-3, 1e-3, 0, 1, 1, 3, 31e-3, 160, 0, out, minTime, stack, 1
7, 0.22, 0.22, 0, 1e-3, 1e-3, 0, 1, 1, 4, 31e-3, 160, 0, out, minTime, stack, 1
8, 0.22, 0.22, 0, 2e-3, 2e-3, 0, 1, 1, 1, 31e-3, 160, 0, out, minTime, stack, 1

%% FFE for comparison
9, 0.22, 0.22, 0, 1.128e-3, 1.128e-3, 0, 4, 1, 1, 31e-3, 200, 0, 0, FFE, stack, 1

%% other EPI for spiral anatomy paper
10, 0.22, 0.22, 0, 0.5642e-3, 0.5642e-3, 0, 36, 1, 1, 31e-3, 200, 0, 0, EPI, stack, 1

%% Spirals for PRISMA with varying slew rates and gradient amplitudes (for Tim)
% slew rate variations
11, 0.22, 0.22, 0, 1.0e-3, 1.0e-3, 0, 1, 1, 4, 31e-3, 130, 0, out, minTime, stack, 1
12, 0.22, 0.22, 0, 1.0e-3, 1.0e-3, 0, 1, 1, 4, 31e-3, 150, 0, out, minTime, stack, 1
13, 0.22, 0.22, 0, 1.0e-3, 1.0e-3, 0, 1, 1, 4, 31e-3, 170, 0, out, minTime, stack, 1

% max grad variations, medium slew rate
14, 0.22, 0.22, 0, 1.0e-3, 1.0e-3, 0, 1, 1, 4, 20e-3, 150, 0, out, minTime, stack, 1
15, 0.22, 0.22, 0, 1.0e-3, 1.0e-3, 0, 1, 1, 4, 40e-3, 150, 0, out, minTime, stack, 1
16, 0.22, 0.22, 0, 1.0e-3, 1.0e-3, 0, 1, 1, 4, 60e-3, 150, 0, out, minTime, stack, 1

% max grad variations, max slew rate
17, 0.22, 0.22, 0, 1.0e-3, 1.0e-3, 0, 1, 1, 4, 20e-3, 170, 0, out, minTime, stack, 1
18, 0.22, 0.22, 0, 1.0e-3, 1.0e-3, 0, 1, 1, 4, 40e-3, 170, 0, out, minTime, stack, 1
19, 0.22, 0.22, 0, 1.0e-3, 1.0e-3, 0, 1, 1, 4, 60e-3, 170, 0, out, minTime, stack, 1

% single to multiple interleaves, medium specs SR/Gmax
20, 0.22, 0.22, 0, 2e-3, 2e-3, 0, 1, 1, 1, 40e-3, 150, 0, out, minTime, stack, 1
21, 0.22, 0.22, 0, 1.2e-3, 1.2e-3, 0, 1, 1, 2, 40e-3, 150, 0, out, minTime, stack, 1
22, 0.22, 0.22, 0, 1.0e-3, 1.0e-3, 0, 1, 1, 3, 40e-3, 150, 0, out, minTime, stack, 1

% single to multiple interleaves, high specs SR/Gmax
23, 0.22, 0.22, 0, 2e-3, 2e-3, 0, 1, 1, 1, 60e-3, 170, 0, out, minTime, stack, 1
24, 0.22, 0.22, 0, 1.2e-3, 1.2e-3, 0, 1, 1, 2, 60e-3, 170, 0, out, minTime, stack, 1
25, 0.22, 0.22, 0, 1.0e-3, 1.0e-3, 0, 1, 1, 3, 60e-3, 170, 0, out, minTime, stack, 1

% The beast
26, 0.22, 0.22, 0, 0.6e-3, 0.6e-3, 0, 1, 1, 4, 60e-3, 200, 0, out, minTime, stack, 1

%% 2D Spiral Diffusion (SPIDI)
501, 0.22, 0.22, 0, 2.5e-3, 2.5e-3, 0, 1, 1, 1, 40e-3, 170, 0, out, minTime, stack, 1
502, 0.22, 0.22, 0, 1.0e-3, 1.0e-3, 0, 1, 1, 4, 40e-3, 170, 0, out, minTime, stack, 1
503, 0.22, 0.22, 0, 1.3e-3, 1.3e-3, 0, 1, 1, 3, 40e-3, 170, 0, out, minTime, stack, 1


%% 2D Spiral fMRI

% only 1 ms slower than SR 200, but no PNS
940, 0.23, 0.23, 0.00, 0.8e-3, 0.8e-3, 0, 4, 1, 1, 31e-3, 160, 0, out, minTime, stack, 1
941, 0.23, 0.23, 0.00, 1.5e-3, 1.5e-3, 0, 4, 1, 1, 31e-3, 160, 0, inout, minTime, stack, 1

% Using new gradient system, parallel mode
910, 0.23, 0.23, 0.00, 0.8e-3, 0.8e-3, 0, 4, 1, 1, 200e-3, 600, 0, out, minTime, stack, 1
911, 0.23, 0.23, 0.00, 1.5e-3, 1.5e-3, 0, 4, 1, 1, 200e-3, 600, 0, inout, minTime, stack, 1

% Using new gradient system, serial mode
930, 0.23, 0.23, 0.00, 0.8e-3, 0.8e-3, 0, 4, 1, 1, 100e-3, 1200, 0, out, minTime, stack, 1
931, 0.23, 0.23, 0.00, 1.5e-3, 1.5e-3, 0, 4, 1, 1, 100e-3, 1200, 0, inout, minTime, stack, 1


%% Competitive EPI (Same k-space area as spiral), old and new slew
9101, 0.23, 0.23, 0, 0.8e-3, 0.8e-3, 0, 4, 1, 1, 31e-3, 200, 0, 0, EPI, stack, 1
9102, 0.23, 0.23, 0, 0.9e-3, 0.9e-3, 0, 4, 1, 1, 31e-3, 200, 0, 0, EPI, stack, 1
9103, 0.23, 0.23, 0, 0.9e-3, 0.9e-3, 0, 4, 1, 1, 31e-3, 160, 0, 0, EPI, stack, 1
9104, 0.23, 0.23, 0, 2.05e-3, 2.05e-3, 0, 1, 1, 1, 31e-3, 160, 0, out, minTime, stack, 1
9105, 0.23, 0.23, 0, 2.5e-3, 2.5e-3, 0, 1, 1, 1, 31e-3, 160, 0, 0, EPI, stack, 1

%% old full slew spirals
920, 0.23, 0.23, 0.00, 0.8e-3, 0.8e-3, 0, 4, 1, 1, 31e-3, 200, 0, out, minTime, stack, 1
921, 0.23, 0.23, 0.00, 1.5e-3, 1.5e-3, 0, 4, 1, 1, 31e-3, 200, 0, inout, minTime, stack, 1


%% AUTOMAP spirals
960, 0.23, 0.23, 0.00, 1e-3, 1e-3, 0, 10, 1, 1, 31e-3, 200, 0, out, minTime, stack, 1

%% spirals for Deep learning demo
961, 0.23, 0.23, 0.00, 2e-3, 2e-3, 0, 1, 1, 2, 31e-3, 200, 0, out, minTime, stack, 1
962, 0.23, 0.23, 0.00, 2e-3, 2e-3, 0, 2, 1, 1, 31e-3, 200, 0, out, minTime, stack, 1
