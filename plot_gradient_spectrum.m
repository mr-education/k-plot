function fh = plot_gradient_spectrum(gradobj)
% Plots gradient waveform and spectrum from GradientWriter object using GradientData.Vis
%
%   output = plot_gradient_spectrum(input)
%
% IN
%
% OUT
%
% EXAMPLE
%   plot_gradient_spectrum
%
%   See also
 
% Author:   Saskia Bollmann & Lars Kasper
% Created:  2020-06-08
% Copyright (C) 2020 Institute for Biomedical Engineering
%                    University of Zurich and ETH Zurich
%
% This file is part of the TAPAS UniQC Toolbox, which is released
% under the terms of the GNU General Public License (GPL), version 3. 
% You can redistribute it and/or modify it under the terms of the GPL
% (either version 3 or, at your option, any later version).
% For further details, see the file COPYING or
%  <http://www.gnu.org/licenses/>.
%

GNew = GradientData();
G = permute(gradobj.Gradients, [1 3 2]);
dt = gradobj.gs.GRADIENT_DWELL;
GNew.time = dt*(0:(numel(G)-1))';
GNew.G = G;
GNew.Vis();

fh = gcf;
